'use strict';

import 'ol/ol.css';
import 'bootstrap/dist/css/bootstrap.css';
import '../scss/main.scss';

import $ from "jquery";

import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import Feature from 'ol/Feature';
import Polyline from 'ol/format/Polyline';
import {defaults as defaultControls, FullScreen} from 'ol/control';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import {Stroke, Style} from 'ol/style';
import {fromLonLat} from 'ol/proj';

const MAP_ZOOM = 0;
const MAP_CENTER = fromLonLat([105.3187561, 61.5240097]);

const $map = $('#map');
const $result = $('#result');
const $only_route = $('#only_route');
const $polyline = $('#polyline');
const $map_width = $('#map_width');
const $map_height = $('#map_height');
const $draw_route = $('[data-action="draw_route"]');
const $draw_and_export = $('[data-action="draw_and_export"]');
const $export_map = $('[data-action="export_map"]');
const $restore_defaults = $('[data-action="restore_defaults"]');
const $map_debug = $('#map_debug');
const $map_controls = $('.ol-control', $map);

const route_source = new VectorSource();
const route_layer = new VectorLayer({
    source: route_source,
    style: new Style({
        stroke: new Stroke({
            width: 3,
            color: [255, 0, 0, 0.8]
        })
    })
});
const view = new View({
    center: MAP_CENTER,
    zoom: MAP_ZOOM
});
const tile_layer = new TileLayer({
    preload: Infinity,
    source: new OSM()
});
const map = new Map({
    target: 'map',
    controls: defaultControls().extend([
        new FullScreen()
    ]),
    layers: [
        tile_layer,
        route_layer
    ],
    view: view
});

function debug(info) {
    $map_debug.text($map_debug.text() + '\n' + info);
}

function handle_error(error) {
    debug(error.stack);
}

function restore_defaults() {
    if ($only_route.prop('checked')) {
        $only_route.prop('checked', false).trigger('change');
    }
    $result.html('');
    $polyline.val('').trigger('change');
    $map_width.val('').trigger('change');
    $map_height.val('').trigger('change');
    view.setCenter(MAP_CENTER);
    view.setZoom(MAP_ZOOM);
}

function draw_route(polyline) {
    const map_size = map.getSize();
    const padding_px = Math.min(...map_size) * 0.1;
    const route = (new Polyline().readGeometry(polyline, {
        // dataProjection: 'EPSG:4326',
        featureProjection: 'EPSG:3857'
    }));
    view.fit(route, {
        padding: [padding_px, padding_px, padding_px, padding_px],
    });

    const route_feature = new Feature({
        geometry: route
    });
    route_source.clear();
    route_source.addFeature(route_feature);
}

function export_map() {
    let img;

    map.once('rendercomplete', function () {
        const canvas = $('.ol-layer canvas').get(0);
        if (!canvas) {
            return debug('ERROR_CANVAS_NOT_FOUND');
        }

        $map_controls.addClass('hidden');

        const src = canvas.toDataURL();
        if (src) {
            img = new Image();
            img.src = src;
            img.setAttribute('id', 'export_result');
            $result.html(img);
        }

        $map_controls.removeClass('hidden');
    });

    function renderSync() {
        map.renderSync();
        setTimeout(function () {
            if (typeof img === 'undefined') {
                renderSync();
            }
        }, 5e3);
    }

    renderSync();
}

$(window).on('resize', function () {
    map.updateSize();
});

$only_route.on('click', function () {
    if (typeof map === 'undefined') {
        return void 0;
    }
    const layers = map.getLayers();
    const $self = $(this);
    if ($self.prop('checked')) {
        layers.remove(tile_layer);
    } else {
        layers.insertAt(0, tile_layer);
    }
});

$map_width.on('change', function () {
    let width = parseInt($(this).val());
    if (isNaN(width) || width <= 0) {
        width = '';
    }
    $map.width(width);
    map.updateSize();
});

$map_height.on('change', function () {
    let height = parseInt($(this).val());
    if (isNaN(height) || height <= 0) {
        height = '';
    }
    $map.height(height);
    map.updateSize();
});

$draw_route.on('click', function () {
    const polyline = $.trim($polyline.val());
    draw_route(polyline);
});

$draw_and_export.on('click', function () {
    const polyline = $.trim($polyline.val());
    draw_route(polyline);
    export_map();
});

$export_map.on('click', function () {
    export_map();
});

$restore_defaults.on('click', function () {
    restore_defaults();
});

window.addEventListener('error', function ($e) {
    handle_error($e.error);
});
